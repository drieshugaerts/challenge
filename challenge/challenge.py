import csv
import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.linear_model import Perceptron
from sklearn.multiclass import OneVsRestClassifier
from sklearn.model_selection import GridSearchCV

# Support vector machine classifier

N = 5

# preprocess data
train_set = pd.read_csv('../radix-challenge/train.csv')
test_set = pd.read_csv('../radix-challenge/test.csv')

x_train = train_set['synopsis']
x_test = test_set['synopsis']
y_train = train_set['genres']

word_vectorizer = CountVectorizer(lowercase=True, token_pattern=r'[^A-Za-z]+', stop_words='english')
tfidf_transformer = TfidfTransformer()
genre_vectorizer = CountVectorizer(lowercase=False, token_pattern=r'[a-zA-Z-]+')

# word count
x_train = word_vectorizer.fit_transform(x_train)
x_test = word_vectorizer.transform(x_test)
y_train = genre_vectorizer.fit_transform(y_train).toarray().astype(float)

#  Term Frequency times inverse document frequency scaling
x = tfidf_transformer.fit_transform(x_train)
x_test = tfidf_transformer.transform(x_test)

# Search for optimal parameters SVM
parameters = {"estimator__alpha": [0.000001, 0.00001, 0.0001, 0.001, 0.01, 0.1, 1]}
model = OneVsRestClassifier(Perceptron())

tuning = GridSearchCV(model, param_grid=parameters)
tuning.fit(x_train, y_train)

# select and train model with optimal parameters
model = OneVsRestClassifier(Perceptron(alpha=tuning.best_params_['estimator__alpha']))
model.fit(x_train, y_train)
y_pred = model.decision_function(x_test)

# write file
f = open('submission.csv', 'w')
with f:
    fieldnames = ['movie_id', 'predicted_genres']
    writer = csv.DictWriter(f, fieldnames=fieldnames)
    writer.writeheader()

    for i in range(x_test.shape[0]):
        most_probable_indices = y_pred[i].argsort()[-1:-(N+1):-1]
        most_probable_genres = np.array(genre_vectorizer.get_feature_names())[most_probable_indices]
        writer.writerow({'movie_id': str(test_set['movie_id'][i]), 'predicted_genres': ' '.join(most_probable_genres)})
